//
//  ViewController.swift
//  MoyaTest
//
//  Created by Sergey Markin on 29.08.17.
//  Copyright © 2017 Test. All rights reserved.
//

import UIKit
import CoreLocation
import RxSwift
import Moya


class ViewController: UIViewController {
    
    private let testLocations: [CLLocationCoordinate2D] = [
        CLLocationCoordinate2D(latitude: 55.760626, longitude: 37.619836),
        CLLocationCoordinate2D(latitude: 55.754542, longitude: 37.621264),
        CLLocationCoordinate2D(latitude: 55.760311, longitude: 37.624889)
    ]
    
    @IBAction private func didTapEncodingTest() {
        let provider = MoyaProvider<StubService>(stubClosure: { _ in .immediate })
        provider.request(.sendLocations(locations: testLocations)) { result in
            switch result {
            case let .success(response):
                let resultObject = try? JSONSerialization.jsonObject(with: response.data)
                print("REQUEST:", response.request?.url?.absoluteString ?? "NIL")
                print("SUCCESS:", resultObject.map { "<\(type(of: $0))> \($0)" } ?? "NIL")
            case let .failure(error):
                print("ERROR:", error)
            }
        }
    }
    
    @IBAction private func didTapIndicatorTest() {
        let provider = RxMoyaProvider<StubService>(stubClosure: { _ in .delayed(seconds: 3) })
        provider.request(.sendLocations(locations: testLocations))
            .mapJSON()
            .showNetworkActivityIndicator()
            .subscribe {
                switch $0 {
                case let .next(response):
                    print("RX SUCCESS:", response)
                case let .error(error):
                    print("RX ERROR:", error)
                default:
                    break
                }
        }
        
    }
    
    

}
