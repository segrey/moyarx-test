//
//  RxIndicatorExtension.swift
//  MoyaTest
//
//  Created by Sergey Markin on 29.08.17.
//  Copyright © 2017 Test. All rights reserved.
//

import RxSwift
import UIKit


extension ObservableType {
    
    func showNetworkActivityIndicator() -> Observable<E> {
        return Observable.create { observer in
            self.dispatchAppNetworkIndicator(visible: true)
            return self.subscribe { event in
                switch event {
                case .next(_):
                    observer.on(event)
                case .error(_):
                    self.dispatchAppNetworkIndicator(visible: false)
                    observer.on(event)
                case .completed:
                    self.dispatchAppNetworkIndicator(visible: false)
                    observer.on(event)
                }
            }
            
        }
    }
    
    private func dispatchAppNetworkIndicator(visible: Bool) {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = visible
        }
    }
    
}
