//
//  StringExtension.swift
//  MoyaTest
//
// Created by Sergey Markin on 29.08.17.
// Copyright (c) 2017 Test. All rights reserved.
//

import Foundation


extension String {
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
    
}
