//
//  LocationArrayEncoding.swift
//  MoyaTest
//
//  Created by Sergey Markin on 29.08.17.
//  Copyright © 2017 Test. All rights reserved.
//

import CoreLocation
import Alamofire
import Moya


struct LocationArrayEncoding: ParameterEncoding {
    
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var req = try urlRequest.asURLRequest()
        guard let params = parameters, !params.isEmpty else {
            return req
        }
        var urlComponents: URLComponents! = URLComponents(url: req.url!, resolvingAgainstBaseURL: false)
        urlComponents.query = (params[StubService.Parameter.locations.rawValue] as? [CLLocationCoordinate2D])?.map {
            "points=\($0.latitude),\($0.longitude)"
            }.joined(separator: "&")
        req.url = urlComponents.url
        return req
    }
    
}
