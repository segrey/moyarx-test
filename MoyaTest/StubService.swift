//
//  StubService.swift
//  MoyaTest
//
//  Created by Sergey Markin on 29.08.17.
//  Copyright © 2017 Test. All rights reserved.
//

import CoreLocation
import Alamofire
import Moya


enum StubService {
    
    enum Parameter: String {
        case locations = "points"
    }
    
    case sendLocations(locations: [CLLocationCoordinate2D])
    
}


extension StubService: TargetType {
    
    var baseURL: URL { return URL(string: "http://api.example.com")! }
    
    var path: String { return "/locations" }
    
    var method: Moya.Method { return .get }
    
    var parameters: [String: Any]? {
        switch self {
        case .sendLocations(let locations):
            return [Parameter.locations.rawValue: locations]
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return LocationArrayEncoding()
    }
    
    var sampleData: Data {
        switch self {
        case .sendLocations(let locations):
            let joinedLocations = locations.map { "{\"lat\":\($0.latitude),\"lon\":\($0.longitude)}" }.joined(separator: ",")
            return "{\"locations\":[\(joinedLocations)]}".utf8Encoded
        }
    }
    
    var task: Task {
        return .request
    }
    
    var validate: Bool {
        switch self {
        case .sendLocations(let locations):
            return !locations.isEmpty
        }
    }
    
}
